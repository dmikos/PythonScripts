#!/usr/bin/python3

import dnsq
import csv

"""
файлы со списком доменов
domain-dc.txt      - система Вебхостинга
domain-ukrpost.txt - система Укрост
domain-test.txt    - список тестовых доменов - 19 шт.
"""

# CONST
# FILE_NAME = 'domain-test'
FILE_NAME = 'domain-dc'
# FILE_NAME = 'domain-ukrpost'
#
FILE_NAME_IN = FILE_NAME + '.txt'
FILE_NAME_OUT = FILE_NAME + '.csv'
SYSTEM_NAME_DICT = {'domain-test.txt':'TEST', 'domain-ukrpost.txt':'Ukrpost', 'domain-dc.txt':'Hosting'}
OUR_DNS_SERVERS = ['213.186.113.10', '195.5.6.10']  # список для определения наш/не-наш primary ДНС
                                                    # на котором правится зона

# Класс описывающий и вычисляющий все данные домена
class md4fm(object):
    def __init__(self, domain_name, system_name):
        self.domain_name = domain_name  # Domain name
        self.system_name = system_name  # UTK system name
        self.pdns = self.prim_ns_name() # primary nameserver
        self.sdns = self.slave_ns_name()    # slave nameservers
        # self.our = [x for x in self.pdns.values()][0] in OUR_DNS_SERVERS    # Признак нашего ведения ДНС или нет - True/False
        self.our = list(self.pdns.values())[0] in OUR_DNS_SERVERS
        self.mxs = self.mx_names()   # MX-ы для домена

    # Primary NS name. Определяем какой NS указан в SOA записи
    def prim_ns_name(self):
        pdns = dnsq.get_primary_nameserver(self.domain_name)
        if pdns is None:    # Если NS в SOA записи не указан
            return {'Has no primary NS':'Has no IP'}
        pdns_ip = dnsq.query_dns(pdns, 'a')
        if pdns_ip != []:
            return {pdns:pdns_ip[0]}
        else:   # Если NS в SOA записи указан, но указанное имя не резолвится
            return {pdns:'Has no IP'}


    # Slave NS name. Определяем какие NS указаны для домена
    def slave_ns_name(self):
        sdns_dict = {}
        sdns = dnsq.query_dns(self.domain_name, 'ns')
        # print('sdns = ', sdns)
        if sdns == []:  # Если NS-ов для домена нет
            return '-Domain has no NS-'
        for unit in sdns:
            unit = unit[:-1]    # убираем точку в конце домена
            #print('unit = ', unit)
            sdns_ip = dnsq.query_dns(unit, 'a')
            # print('sdns_ip = ', sdns_ip)
            if sdns_ip != []:
                sdns_dict[unit] = sdns_ip[0]
            else:   # Если NS имя не резолвится
                sdns_dict[unit] = 'Has no IP'
        return sdns_dict

    # MX_names
    def mx_names(self):
        mxs_dict = {}
        mxs = dnsq.query_dns(self.domain_name, 'mx')
        for unit in mxs:
            unit = unit.split(' ')[1][:-1]
            mx_ip = dnsq.query_dns(unit, 'a')
            if mx_ip != []:
                mxs_dict[unit] = mx_ip[0]
            else:   # Если MX запись не резолвится
                mxs_dict[unit] = 'Has no IP'
        return mxs_dict


if __name__ == '__main__':
    # Сохраняем данные в текстовый файл, формат csv, аналогичный файлу созданному из ПО LibreOffice
    # Поля отделяем символом ',', строки с переносами выделяем символами '"'.
#    with open(FILE_NAME_IN, 'rt') as f_in, open(FILE_NAME_OUT, 'wt') as f_out:
#        header = 'Имя домена,Имя сервиса,SOA NS Name,SOA NS IP,Наша система,NS-ы,MX-ы'
#        f_out.write(header + '\n')
#        for line in f_in:
#            system_name = SYSTEM_NAME_DICT[FILE_NAME_IN]
#            dn_obj = md4fm(line.strip(), system_name)
#            #print('Имя домена - ', dn_obj.domain_name)
#            #print('Имя сервиса - ', dn_obj.system_name)
#            #print('SOA NS - ', dn_obj.pdns)
#            #print('Наша система - ', dn_obj.our)
#            #print('Все NS - ', dn_obj.sdns)
#            #print('MX-ы - ', dn_obj.mxs)
#            #print()
#            #
#            #print('Prim NS - ', '\n'.join("{!s} = {!s}".format(key, val) for (key, val) in dn_obj.sdns.items()))
#            row = ','.join([str(dn_obj.domain_name),
#                             str(dn_obj.system_name),
#                             ','.join("{!s},{!s}".format(key, val) for (key, val) in dn_obj.pdns.items()),
#                             str(dn_obj.our),
#                             '"'+str(dn_obj.sdns)[1:-1].replace(', ','\n')+'"',
#                             '"'+str(dn_obj.mxs)[1:-1].replace(', ','\n')+'"'
#                             ])
#            f_out.write(row + '\n')

    # Сохраняем данные в csv файл используя библиотеку csv. "import csv"
    # Поля отделяем символом ',', строки с переносами выделяем символами '"
    header = ('Имя домена', 'Имя сервиса', 'SOA NS Name', 'SOA NS IP', 'Наша система','NS-ы', 'MX-ы')

    with open(FILE_NAME_IN, 'rt') as f_in, open(FILE_NAME_OUT, 'wt') as f_out:
        writer = csv.writer(f_out, quoting = csv.QUOTE_ALL)    # csv.QUOTE_ALL-Instructs writer objects to quote all fields)
        writer.writerow(header)

        for line in f_in:
            system_name = SYSTEM_NAME_DICT[FILE_NAME_IN]
            dn_obj = md4fm(line.strip(), system_name)

            row = list()
            row.append(dn_obj.domain_name)
            row.append(dn_obj.system_name)
            for (key, val) in dn_obj.pdns.items():
                row.append(key)
                row.append(val)
            row.append(dn_obj.our)
            row.append(str(dn_obj.sdns)[1:-1].replace(', ', '\n'))
            row.append(str(dn_obj.mxs)[1:-1].replace(', ', '\n'))
            writer.writerow(row)
